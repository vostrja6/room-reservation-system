//package cz.cvut.kbss.ear.ctm.service;
//
//import cz.cvut.kbss.ear.ctm.config.TestPersistenceConfig;
//import cz.cvut.kbss.ear.ctm.config.TestServiceConfig;
//import org.junit.runner.RunWith;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.transaction.annotation.Transactional;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {TestServiceConfig.class, TestPersistenceConfig.class})
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@Transactional(transactionManager = "txManager")
//public abstract class BaseServiceTestRunner {
//
//}