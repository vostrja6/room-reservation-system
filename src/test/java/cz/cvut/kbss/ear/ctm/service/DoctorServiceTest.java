//package cz.cvut.kbss.ear.ctm.service;
//
//import cz.cvut.kbss.ear.ctm.config.Environment;
//import cz.cvut.kbss.ear.ctm.config.Generator;
//import cz.cvut.kbss.ear.ctm.exception.AlreadyAddedReservationException;
//import cz.cvut.kbss.ear.ctm.model.Person;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.List;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
//public class DoctorServiceTest extends BaseServiceTestRunner {
//    @Autowired
//    private PersonService doctorService;
//
//    @Autowired
//    private PatientService patientService;
//
//    @Test
//    public void addOnePatient() throws Exception, AlreadyAddedReservationException {
//        Doctor doctor = Generator.generateDoctor();
//        Person patient = Generator.generatePatient1();
//
//        patientService.persist(patient);
//        doctorService.persist(doctor);
//
//        Environment.setCurrentUser(doctor);
//        doctorService.addPatient(patient);
//
//        Doctor dbDoctor = doctorService.findByEmail(doctor.getEmail());
//        Person dbPatient = patientService.findByEmail(patient.getEmail());
//
//        assertTrue(dbDoctor.getPatients().size() == 1);
//        assertTrue(dbPatient.getPatientDoctors().size() == 1);
//        assertTrue(dbDoctor.getPatients().get(0).equals(dbPatient));
//        assertTrue(dbPatient.getPatientDoctors().get(0).equals(dbDoctor));
//    }
//
//    @Test(expected = AlreadyAddedReservationException.class)
//    public void addAlreadyAddedPatient() throws Exception, AlreadyAddedReservationException {
//        Doctor doctor = Generator.generateDoctor();
//        Person patient = Generator.generatePatient1();
//
//        patientService.persist(patient);
//        doctorService.persist(doctor);
//
//        doctorService.addPatient(patient);
//
//        Doctor dbDoctor = doctorService.findByEmail(doctor.getEmail());
//        Person dbPatient = patientService.findByEmail(patient.getEmail());
//
//        assertTrue(dbDoctor.getPatients().size() == 1);
//        assertTrue(dbPatient.getPatientDoctors().size() == 1);
//        assertTrue(dbDoctor.getPatients().get(0).equals(dbPatient));
//        assertTrue(dbPatient.getPatientDoctors().get(0).equals(dbDoctor));
//
//        doctorService.addPatient(patient);//add same patient
//    }
//
//    @Test
//    public void getDto() throws Exception, AlreadyAddedReservationException, AlreadyRemovedPatientException
//    {
//        Doctor doctor = Generator.generateDoctor();
//        Person patient = Generator.generatePatient1();
//
//        patientService.persist(patient);
//        doctorService.persist(doctor);
//
//        Environment.setCurrentUser(doctor);
//        doctorService.addPatient(patient);
//
//        Doctor dbDoctor = doctorService.findByEmail(doctor.getEmail());
//        Person dbPatient = patientService.findByEmail(patient.getEmail());
//
//        assertTrue(dbDoctor.getPatients().size() == 1);
//        assertTrue(dbPatient.getPatientDoctors().size() == 1);
//        assertTrue(dbDoctor.getPatients().get(0).equals(dbPatient));
//        assertTrue(dbPatient.getPatientDoctors().get(0).equals(dbDoctor));
//
//        List<DoctorDto> list = doctorService.findAllDto();
//
//        assertTrue(list.size() == 1);
//        assertEquals(dbDoctor.getEmail(), list.get(0).getEmail());
//    }
//}