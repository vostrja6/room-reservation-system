//package cz.cvut.kbss.ear.ctm.service;
//
//import cz.cvut.kbss.ear.ctm.config.Environment;
//import cz.cvut.kbss.ear.ctm.config.Generator;
//import cz.cvut.kbss.ear.ctm.exception.AlreadyAddedReservationException;
//import cz.cvut.kbss.ear.ctm.model.Address;
//import cz.cvut.kbss.ear.ctm.model.Doctor;
//import cz.cvut.kbss.ear.ctm.model.Person;
//import cz.cvut.kbss.ear.ctm.model.enums.HealthCareInsurer;
//import org.junit.Assert;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import static org.junit.Assert.assertTrue;
//
//public class PatientServiceTest extends BaseServiceTestRunner {
//
//    @Autowired
//    private PatientService patientService;
//
//    @Autowired
//    private AddressService addressService;
//
//    @Autowired
//    private PersonService doctorService;
//
//    private final String PATIENT_EMAIL = "test@fel.cvut.cz";
//
//    private Person createPatient() {
//        return new Person(
//                "Jakub",
//                "Vančo",
//                PATIENT_EMAIL,
//                "777258958",
//                "9702272558", "abc",
//                HealthCareInsurer.VZP,
//                new Address("Test-Žatec", "Lounska", "2435", "43801"));
//    }
//
//
//    @Test
//    public void updateWholePatient() throws Exception {
//        Person p = createPatient();
//
//        patientService.persist(p); //Create patient into database
//
//        Person createdPatient = patientService.findByEmail(PATIENT_EMAIL); //Get the patient we just created
//        createdPatient.setPhoneNumber("777888555"); //Change his phone number
//        createdPatient.setFirstName("Kuba"); //Change his name
//
//        patientService.update(createdPatient); //Update patient
//
//        Person updatedPatient = patientService.findByEmail(PATIENT_EMAIL); //Reload the updated patient
//
//        Assert.assertTrue(createdPatient.equals(updatedPatient));
//
//    }
//
//    @Test
//    public void updatePatientAddress_addressNotExists() throws Exception {
//        Address newAddress = new Address("Test-Praha", "Evropská", "631", "16000");
//
//        Person p = createPatient();
//        patientService.persist(p); //Create patient into database
//
//        Person createdPatient = patientService.findByEmail(PATIENT_EMAIL); //Get the patient we just created
//        createdPatient.setAddress(newAddress); //Change his address to new address
//
//        patientService.update(createdPatient); //Update patient
//        Person updatedPatient = patientService.findByEmail(PATIENT_EMAIL); //Reload the updated patient
//
//        Assert.assertTrue(updatedPatient.getAddress().equals(newAddress));
//        Assert.assertTrue(createdPatient.equals(updatedPatient));
//
//    }
//
//    @Test
//    public void updatePatientAddress_addressExists() throws Exception {
//        Address newAddress = new Address("Test-Praha", "Evropská", "631", "16000");
//        addressService.persist(newAddress);
//
//        Person p = createPatient();
//        patientService.persist(p); //Create patient into database
//
//        Person createdPatient = patientService.findByEmail(PATIENT_EMAIL); //Get the patient we just created
//        createdPatient.setAddress(newAddress); //Change his address to new address
//
//        patientService.update(createdPatient); //Update patient
//        Person updatedPatient = patientService.findByEmail(PATIENT_EMAIL); //Reload the updated patient
//
//        Assert.assertTrue(updatedPatient.getAddress().equals(newAddress));
//        Assert.assertTrue(createdPatient.equals(updatedPatient));
//    }
//
//
//    public void removeDoctor() throws Exception, AlreadyAddedReservationException {
//        Doctor doctor = Generator.generateDoctor();
//        Person patient = Generator.generatePatient1();
//
//        patientService.persist(patient);
//        doctorService.persist(doctor);
//
//        Environment.setCurrentUser(doctor);
//
//        doctorService.addPatient(patient);
//
//        Doctor dbDoctor = doctorService.findByEmail(doctor.getEmail());
//        Person dbPatient = patientService.findByEmail(patient.getEmail());
//
//        assertTrue(dbDoctor.getPatients().size() == 1);
//        assertTrue(dbPatient.getPatientDoctors().size() == 1);
//        assertTrue(dbDoctor.getPatients().get(0).equals(dbPatient));
//        assertTrue(dbPatient.getPatientDoctors().get(0).equals(dbDoctor));
//
//        Environment.setCurrentUser(patient);
//
//        assertTrue(dbDoctor.getPatients().size() == 0);
//        assertTrue(dbPatient.getPatientDoctors().size() == 0);
//    }
//}