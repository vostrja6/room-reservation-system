'use strict';

import Reflux from "reflux";
import Actions from "../actions/Actions";
import Ajax from "../utils/Ajax";

let personLoading = false;
const BASE_URL = 'rest/persons';
const BASE_URL_WITH_SLASH = 'rest/persons/';

export default class UserStore extends Reflux.Store {

    constructor() {
        super();
        this.listenables = Actions;
    }

    onLoadUser() {
        Ajax.get('rest/persons/current').end((user) => {
            this.setState({user: user, loaded: true});
        }, () => {
            this.setState({loaded: true});
        });
    }

    onUpdateUser(user, onSuccess, onError) {
        Ajax.put('rest/persons/current', user).end(onSuccess, onError);
    }

    onLoadMyReservations(onSuccess) {
        if (personLoading) {
            return;
        }
        personLoading = true;
        Ajax.get(BASE_URL_WITH_SLASH + 'myReservations').end((data) => {
            personLoading = false;
            this.setState({reservations: data});
            if (onSuccess) {
                onSuccess();
            }
        }, () => {
            personLoading = false;
            this.setState({reservations: []});
        });
    }
}
