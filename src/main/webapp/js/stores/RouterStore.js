'use strict';

/**
 * Manages passing payloads on routing transition.
 *
 * For example, when one wants to pass and object when transitioning to another route, this store will be used to store
 * the payload object and the target route handler can ask for it.
 */
class RouterStore {

    constructor() {
        this.transitionPayload = {};
    }

    setTransitionPayload(routeName, payload) {
        if (!payload) {
            delete this.transitionPayload[routeName];
        } else {
            this.transitionPayload[routeName] = payload;
        }
    }

    /**
     * Gets the specified route's payload, if there is any.
     * @param routeName Route name
     * @return {*} Route transition payload or null if there is none for the specified routeName
     */
    getTransitionPayload(routeName) {
        return this.transitionPayload[routeName];
    }
}

const INSTANCE = new RouterStore();

export default INSTANCE;
