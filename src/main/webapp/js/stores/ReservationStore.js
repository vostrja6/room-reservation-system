'use strict';

import Reflux from "reflux";
import Actions from "../actions/Actions";
import Ajax from "../utils/Ajax";
import Utils from "../utils/Utils";

const BASE_URL = 'rest/reservations';
const BASE_URL_WITH_SLASH = 'rest/reservations/';

// When reports are being loaded, do not send the request again
let reservations = false;
let rooms = false;
let reservation = false;

export default class ReservationStore extends Reflux.Store {

    constructor() {
        super();
        this.listenables = Actions;
    }

    onLoadMyReservations(onSuccess) {
        if (reservations) {
            return;
        }
        reservations = true;
        Ajax.get(BASE_URL_WITH_SLASH + "myReservations").end((data) => {
            reservations = false;
            this.setState({reservations: data});
            if (onSuccess) {
                onSuccess();
            }
        }, () => {
            reservations = false;
            this.setState({reservations: []});
        });
    }

    onLoadAllReservations(onSuccess) {
        if (reservations) {
            return;
        }
        reservations = true;
        Ajax.get(BASE_URL).end((data) => {
            reservations = false;
            this.setState({reservations: data});
            if (onSuccess) {
                onSuccess();
            }
        }, () => {
            reservations = false;
            this.setState({reservations: []});
        });
    }

    onSetConfirmReservation(id, onSuccess) {
        Ajax.put(BASE_URL_WITH_SLASH + "confirm/" + id).end((data) => {
            if (onSuccess) {
                onSuccess();
            }
            this.onLoadMyReservations();
        }, ()=> {});
    }

    onSetCreatedReservation(id, onSuccess) {
        Ajax.put(BASE_URL_WITH_SLASH + "set/created/" + id).end((data) => {
            if (onSuccess) {
                onSuccess();
            }
            this.onLoadMyReservations();
        }, ()=> {});
    }

    onSetWaitingReservation(id, onSuccess) {
        Ajax.put(BASE_URL_WITH_SLASH + "set/waiting/" + id).end((data) => {
            if (onSuccess) {
                onSuccess();
            }
            this.onLoadMyReservations();
        }, ()=> {});
    }

    onSetPassedReservation(id, onSuccess) {
        Ajax.put(BASE_URL_WITH_SLASH + "set/passed/" + id).end((data) => {
            if (onSuccess) {
                onSuccess();
            }
            this.onLoadMyReservations();
        }, ()=> {});
    }

    onRemoveReservation(id, onSuccess) {
        Ajax.del(BASE_URL_WITH_SLASH + id).end((data) => {
            if (onSuccess) {
                onSuccess();
            }
            this.onLoadMyReservations();
        }, () => {}
            );
    }

    onCreateReservations(reservations, onSuccess, onError) {
        Ajax.post(BASE_URL, reservations).end((data, resp) => {
            if (onSuccess) {
                onSuccess();
            }
            this.onLoadMyReservations();
        }, onError);
    }

    onFindReservation(date, time, roomId, onSuccess) {
        Ajax.get(BASE_URL_WITH_SLASH + 'date=' + date + '/time=' + time + '/room=' + roomId).end((data) => {
            reservations = false;
            this.setState({reservation: data});
            if (onSuccess) {
                onSuccess();
            }
        }, () => {
            reservations = false;
            this.setState({reservation: []});
        });
    }

    onCreateReservation(reservation, onSuccess, onError) {
        Ajax.post(BASE_URL, reservation).end((data, resp) => {
            if (onSuccess) {
                const id = Utils.extractIdFromLocationHeader(resp);
                onSuccess(id);
            }
            this.onLoadMyReservations();
        }, onError);
    }
}
