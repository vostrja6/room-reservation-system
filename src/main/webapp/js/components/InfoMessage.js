import React from 'react';
import {Alert} from 'react-bootstrap';

const InfoMessage = () => {
    return <Alert bsStyle="success">If you are seeing this message, your system has all the necessary tools and
        libraries
        and is prepared for developing project.</Alert>;
};

export default InfoMessage;
