'use strict';

import React from "react";
import PropTypes from "prop-types";
import {Alert, Button} from "react-bootstrap";
import {FormattedMessage} from "react-intl";

import I18nWrapper from "../i18n/I18nWrapper";
import injectIntl from "../utils/injectIntl";
import Routing from "../utils/Routing";

/**
 * Shows alert with message informing that a resource could not be found.
 *
 * Closing the alert transitions the user to the application's home.
 */
class ResourceNotFound extends React.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
    }

    onClose = () => {
        Routing.transitionToHome();
    };

    render() {
        let text;
        if (this.props.identifier) {
            text = <FormattedMessage id='notfound.msg-with-id'
                                     values={{resource: this.props.resource, identifier: this.props.identifier}}/>;
        } else {
            text = <FormattedMessage id='notfound.msg' values={{resource: this.props.resource}}/>;
        }
        return <Alert bsStyle='danger' onDismiss={this.onClose}>
            <h4>{this.i18n('notfound.title')}</h4>
            <p>{text}</p>
            <p>
                <Button onClick={this.onClose}>{this.i18n('close')}</Button>
            </p>
        </Alert>;
    }
}

ResourceNotFound.propTypes = {
    resource: PropTypes.string.isRequired,
    identifier: PropTypes.any
};

export default injectIntl(I18nWrapper(ResourceNotFound));
