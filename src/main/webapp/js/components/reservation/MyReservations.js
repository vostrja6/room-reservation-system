import React from "react";

import {Button} from "react-bootstrap";
import injectIntl from "../../utils/injectIntl";
import I18nWrapper from "../../i18n/I18nWrapper";
import Mask from "../Mask";
import PropTypes from "prop-types";
import ReactTable from 'react-table'

class MyReservations extends React.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
    }

    _setUpReservation() {
        var date_now = new Date();

        console.log("IM IN SETUP");

        for (let i = 0; i < this.props.reservations.length; i++) {
            console.log("NEXT RESERVATION");
            console.log(this.props.reservations);
            // date format: 16/07/19
            // time format: 15:30
            var res_dateFull = this.props.reservations[i].date;
            var res_timeFromFull = this.props.reservations[i].timeFrom;

            var res_date = parseInt(res_dateFull.substring(0, 2));
            var res_month = parseInt(res_dateFull.substring(3, 5));
            var res_year = parseInt('20' + res_dateFull.substring(6, 8));
            var res_timeFromHours = parseInt(res_timeFromFull.substring(0, 2));
            var res_timeFromMin = parseInt(res_timeFromFull.substring(3, 5));

            var date_res = new Date(res_year, (res_month-1), res_date, res_timeFromHours, res_timeFromMin);

            var diff = parseInt((date_res-date_now)/60000);

            console.log("Time: " + diff);
            console.log(this.props.reservations[i].id + " status before change: " + this.props.reservations[i].status);

            if (this.props.reservations[i].status === 0) {
                this.props.reservations[i].status = 'Created';
            }
            else if (this.props.reservations[i].status === 1) {
                this.props.reservations[i].status = 'Waiting';
            }
            else if (this.props.reservations[i].status === 2) {
                this.props.reservations[i].status = 'Confirmed';
            }
            else if (this.props.reservations[i].status === 3) {
                this.props.reservations[i].status = 'Passed'
            }

            console.log(this.props.reservations[i].id + " status change: " + this.props.reservations[i].status);

            if (diff < 30) {
                if (!(this.props.reservations[i].status === 'Passed')) {
                    this.props.actions.setPassedReservation(this.props.reservations[i].id);
                    this.props.reservations[i].status = 'Passed';
                    console.log(this.props.reservations[i].status + "request set passed + change " + diff);
                }
                else {
                    console.log("No cares given");
                }
            }
            else if ((30 <= diff) && (diff <= 60)) {
                if (!(this.props.reservations[i].status === 'Waiting')) {
                    if(this.props.reservations[i].status === 'Confirmed') {
                        console.log("Do nothing");
                    }
                    else {
                        this.props.actions.setWaitingReservation(this.props.reservations[i].id);
                        this.props.reservations[i].status = 'Waiting';
                        console.log(this.props.reservations[i].status + "request set waitin+ change " + diff);
                    }
                }
                else {
                    console.log("No cares given");
                }
            }
            else if (diff > 60) {
                if (!(this.props.reservations[i].status === 'Created')) {
                    this.props.actions.setCreatedReservation(this.props.reservations[i].id);
                    this.props.reservations[i].status = 'Created';
                    console.log(this.props.reservations[i].status + "request set created+ change " + diff);
                }
                else {
                    console.log("No cares given");
                }
            }

            console.log("Final state: ");
            console.log(this.props.reservations[i].id + " status change: " + this.props.reservations[i].status);
            console.log(this.props.reservations);
            console.log("----------------------------------------");
        }
    }

    render() {
        if (!this.props.reservations) {
            return <Mask text='Loading...'/>;
        }
        console.log("RENDER");
        this._setUpReservation();

        return <div>
            <ReactTable
                data={this.props.reservations}
                noDataText="You have no reservations yet."
                columns={[
                    {
                        Header: "Reservation",
                        columns: [
                            {
                                Header: "Date",
                                id: "date",
                                accessor: d => d.date,
                            },
                            {
                                Header: "Time from",
                                id: "timeFrom",
                                accessor: d => d.timeFrom,
                            },
                            {
                                Header: "Time to",
                                id: "timeTo",
                                accessor: d => d.timeTo,
                            },
                            {
                                Header: "Status",
                                id: "status",
                                accessor: d => d.status,
                            },
                            {
                                Header: "Room",
                            }
                        ]
                    },
                    {
                        Header: "Actions",
                        columns: [
                            {
                                accessor: "id",
                                Cell: d => (this._renderButtons(d.value, d.row.status))

                            },
                        ]
                    },
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
                filterable
            />
            <br />
        </div>;
    }

    _renderButtons(reservationId, status)
    {
        if (status === 'Created') {
            //created
            return <div>
                <Button
                    bsStyle='danger'
                    onClick={() => this.props.actions.removeReservation(reservationId)}
                >
                    Delete
                </Button>
            </div>
        }
        else if (status === 'Waiting') {
            //waiting
            return <div>
                <Button
                    bsStyle='success'
                    onClick={() => this.props.actions.setConfirmReservation(reservationId)}
                >
                    Confirm
                </Button>
                <Button
                    bsStyle='danger'
                    onClick={() => this.props.actions.removeReservation(reservationId)}
                >
                    Delete
                </Button>
            </div>;
        }
        else if (status === 'Confirmed') {
            //confirmed
            return <div>
                <Button
                    bsStyle='success'
                    disabled={true}
                >
                    Confirmed
                </Button>
            </div>
        }
        else {
            return <div></div>
        }
    }
}

MyReservations.propTypes = {
    reservations: PropTypes.array,
    actions: PropTypes.object.isRequired
};

export default injectIntl(I18nWrapper(MyReservations));
