'use strict';

import React from "react";
import Reflux from "reflux";

import Actions from "../../actions/Actions";
import ReservationStore from "../../stores/ReservationStore";
import MyReservations from "./MyReservations";
import injectIntl from "../../utils/injectIntl";
import I18nWrapper from "../../i18n/I18nWrapper";
import MessageWrapper from "../misc/MessageWrapper";
import Routes from "../../utils/Routes";
import RoutingRules from "../../utils/RoutingRules";
import UserStore from "../../stores/UserStore";

class MyReservationsController extends Reflux.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
        this.state = {};
        this.stores = [ReservationStore, UserStore];
        this.storeKeys = ['reservations', 'user'];
    }

    componentWillMount() {
        super.componentWillMount();
        RoutingRules.execute(Routes.myReservations.name);
        this.setState({reservations: null});
        Actions.loadUser();
        Actions.loadMyReservations();
    }

    onRemoveReservation = (reservationId) => {
        console.log('remove reservation');
        Actions.removeReservation(reservationId, () => this.props.showSuccessMessage('Reservation successfully removed.'));
        Actions.loadAllRooms();
    };

    onSetConfirmReservation = (reservationId) => {
        console.log('confirm reservation');
        Actions.setConfirmReservation(reservationId, () => this.props.showSuccessMessage('Reservation successfully confirmed.'));
    };

    onSetWaitingReservation = (reservationId) => {
        console.log('waiting reservation');
        Actions.setWaitingReservation(reservationId, status);
    };

    onSetPassedReservation = (reservationId) => {
        console.log('passed reservation');
        Actions.setPassedReservation(reservationId, status);
    };

    onSetCreatedReservation = (reservationId) => {
        console.log('created reservation');
        Actions.setCreatedReservation(reservationId, status);
    };

    onUpdateReservation = (reservationId) => {
        console.log('update reservation');
    };

    render() {
        const actions = {
            removeReservation: this.onRemoveReservation,
            setConfirmReservation: this.onSetConfirmReservation,
            setCreatedReservation: this.onSetCreatedReservation,
            setWaitingReservation: this.onSetWaitingReservation,
            setPassedReservation: this.onSetPassedReservation,
        };
        return <MyReservations reservations={this.state.reservations} actions={actions} myReservations={true}/>;
    }
}

export default injectIntl(I18nWrapper(MessageWrapper(MyReservationsController)));
