'use strict';

import React from "react";
import Reflux from "reflux";
import PropTypes from "prop-types";
import JsonLdUtils from "jsonld-utils";

import Actions from "../../actions/Actions";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import MessageWrapper from "../misc/MessageWrapper";
import UserStore from "../../stores/UserStore";
import Reservation from "./Reservation";
import Routes from "../../utils/Routes";
import Routing from "../../utils/Routing";
import RoutingRules from "../../utils/RoutingRules";
import RoomStore from "../../stores/RoomStore"
import ReservationStore from "../../stores/ReservationStore";

class ReservationController extends Reflux.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
        this.state = {
            loading: false,
            notFound: false
        };
        this.stores = [UserStore, RoomStore, ReservationStore];
        this.storeKeys = ['user', 'rooms', 'reservation'];
    }

    componentWillMount() {
        super.componentWillMount();
        console.log("mounting controller");
        RoutingRules.execute(Routes.reservation.name);
        this.setState({loading: true});
        Actions.loadUser();
        Actions.loadAllRooms();
        this.state.reservation = '';
    }

    _onSuccees = () =>
    {
        this.setState({loading: false});
    };

    onCreateReservation = (reservationData) => {
        console.log(reservationData);
        Actions.createReservation(reservationData, () => Routing.transitionTo(Routes.myReservations));
    };

    onFindReservation = (date, time, roomId) => {
        Actions.findReservation(date, time, roomId, () => this.onRenderMessage());
        //() => {}
    };

    onRenderMessage = () => {
        if (this.state.reservation) {
            this.state.notFound = false;
            this.props.showSuccessMessage('Reservation does exists. Check rooms reservations for more info.');
        }
        else if (this.state.reservation === null) {
            this.state.notFound = true;
            this.props.showSuccessMessage('Room is not reserved for this date and time. You can create reservation');
        }
        else {
            this.state.notFound = false;
            this.props.showSuccessMessage("Something went wrong");
        }
    }

    render() {
        const actions = {
            createReservation: this.onCreateReservation,
            findReservation: this.onFindReservation,
        };

        if(!this.state.loading && !this.state.rooms && !this.state.user)
        {
            return <div>Loading</div>;
        }


        console.log("render reservation");
        return <Reservation
            actions={actions}
            user={this.state.user}
            rooms={this.state.rooms}
            reservation={this.state.reservation}
            notFound={this.state.notFound}
        />;
    }
}


export default injectIntl(I18nWrapper(MessageWrapper(ReservationController)));
