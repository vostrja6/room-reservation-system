import React from "react";
import PropTypes from "prop-types";
import {Alert, Button, Panel} from "react-bootstrap";

import Ajax from "../../utils/Ajax";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Input from "../HorizontalInput";
import Routing from "../../utils/Routing";
import Routes from "../../utils/Routes";
import Logger from "../../utils/Logger";
import Select from "react-select";
import LoadingWrapper from "../misc/LoadingWrapper";
import MessageWrapper from "../misc/MessageWrapper";
import PersonValidator from "../../validator/PersonValidator";

class AddDoctor extends React.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
        this.state = {
            firstName: '',
            lastName: '',
            username: '',
            password: '',
            passwordConfirm: '',
            birthNumber: '',
            phoneNumber: '',

            degreeAfter: '',
            degreeBefore: '',
            description: '',
            crn: '',

            alertVisible: false,
            errorMessage: '',
            usernameExists: false,
            birthNumberExists: false,
            mask: false
        };
    }

    componentWillUnmount() {
        if (this.alertTimeout) {
            clearTimeout(this.alertTimeout);
        }
    }

    onChange = (e) => {
        const change = {};
        change[e.target.name] = e.target.value;
        this.setState(change);
    };

    _onUsernameChange = (e) => {
        this.onChange(e);
        const value = e.target.value;
        let username = document.getElementById("username");

        if (this.state.username !== e.target.value) {
            Ajax.get('rest/persons/exists?username=' + value).end((data) => {
                if (data === 'true') {
                    this.setState({usernameExists: true});
                    username.title = this.i18n('profile.username.exists');
                } else {
                    this.setState({usernameExists: false});
                    username.title = this.i18n('register.username-not-matching-tooltip');
                }
            });
        }
    };

    onKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.register();
        }
    };

    showAlert = (msg) => {
        this.setState({alertVisible: true, errorMessage: msg, mask: false});
        this.alertTimeout = setTimeout(this.dismissAlert, 5000);
    };

    _onBirthNumberChange = (e) => {
        this.onChange(e);
        const value = e.target.value;
        let birthNumber = document.getElementById("birthNumber");
        if (this.state.birthNumber !== e.target.value) {
            Ajax.get('rest/patients/existsBirthNumber?birthNumber=' + value).end((data) => {
                if (data === 'true') {
                    this.setState({birthNumberExists: true});
                    birthNumber.title = this.i18n('profile.birthNumber.exists');
                } else {
                    this.setState({birthNumberExists: false});
                    birthNumber.title = this.i18n('register.birthnumber-not-matching-tooltip');
                }
            });
        }
        console.log(this.state);
    };

    dismissAlert = () => {
        this.setState({alertVisible: false});
        this.alertTimeout = null;
    };

    isValid = () => {
        return PersonValidator.isValid(this.state);
    };

    _addDoctor = () => {
        if (!this.isValid()) {
            return;
        }
        const userData = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.username,
            password: this.state.password,
            phoneNumber: this.state.phoneNumber,
            crn: this.state.crn,
            description: this.state.description,
            degreeAfter: this.state.degreeAfter,
            degreeBefore: this.state.degreeBefore,
            webPage: ''
        };
        Ajax.post('rest/admin', userData).end(function (body, resp) {
            if (resp.status === 201) {
                this.setState({alertVisible: true, errorMessage: 'Doktor přidán', mask: false});
                this.alertTimeout = setTimeout(this.dismissAlert, 10000);
                Routing.transitionTo(Routes.dashboard);
            }
        }.bind(this), function (err) {
            this.showAlert(err.message ? err.message : 'Unknown error.');
        }.bind(this));
        this.setState({mask: true});
    };

    render() {
        const panelCls = this.state.alertVisible ? 'register-panel expanded' : 'register-panel',
            mask = this.state.mask ? (<Mask text={this.i18n('register.mask')}/>) : null;

        var numberRegex = /^[0-9]+$/;
        var nameRegex = /^[a-zěščřžýáíéňA-ZĚŠČŘŽÝÁÍÉŇ]+$/;
        var degreeRegex = /^[a-zěščřžýáíéňA-ZĚŠČŘŽÝÁÍÉŇ0-9 \.]+$/;
        var usernameRegex = /^[a-zA-Z0-9]+$/;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        // const healthCareInsurerPass = this.state.healthCareInsurer.length <= 255 && this.state.healthCareInsurer.match(nameRegex);
        const firstnamePass = this.state.firstName.length <= 30 && this.state.firstName.match(nameRegex);
        const lastnamePass = this.state.lastName.length <= 30 && this.state.lastName.match(nameRegex);
        const usernamePass = this.state.username.length <= 40 && this.state.username.match(emailRegex);
        const phonenumbPass = this.state.phoneNumber.length == 9 && this.state.phoneNumber.match(numberRegex);

        const degreeAfter = this.state.degreeAfter.length <= 30 && this.state.degreeAfter.match(degreeRegex);
        const degreeBefore = this.state.degreeBefore.length <= 10 && this.state.degreeBefore.match(degreeRegex);
        const description = this.state.description.length <= 150 && this.state.description.match(degreeRegex);
        const crn = this.state.crn.length <= 15 && this.state.crn.match(degreeRegex);

        return <form className='form-horizontal' style={{margin: '0.5em 0 0 0'}}>
                {this.renderAlert()}
                <div className='row'>
                    <div className='col-xs-6'>
                        <h4>Osobní údaje</h4>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input type='text' name='firstName' label={this.i18n('register.firstName') + '*'}
                               value={this.state.firstName}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!firstnamePass && this.state.firstName.length > 0 ? 'error' : null}
                               title={!firstnamePass ? this.i18n('register.firstName-not-matching-tooltip') : null}/>
                        <span
                            className="error text-danger pull-right">{(!firstnamePass && this.state.firstName.length > 0) ? this.i18n('register.firstName-not-matching-tooltip') : null}</span>
                    </div>
                    <div className='col-xs-6'>
                        <Input type='text' name='lastName' label={this.i18n('register.lastName') + '*'}
                               value={this.state.lastName}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!lastnamePass && this.state.lastName.length > 0 ? 'error' : null}
                               title={!lastnamePass ? this.i18n('register.lastName-not-matching-tooltip') : null}/>
                        <span
                            className="error text-danger pull-right">{(!lastnamePass && this.state.lastName.length > 0) ? this.i18n('register.lastName-not-matching-tooltip') : null}</span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input type='text' name='username' label={this.i18n('register.username') + '*'}
                               title={!usernamePass ? this.i18n('register.username-not-matching-tooltip') : null}
                               value={this.state.username}
                               validation={this.state.usernameExists || !usernamePass && this.state.username.length > 0 ? 'error' : null}
                               labelWidth={4} inputWidth={8} onChange={this._onUsernameChange}/>
                        <span
                            className="error text-danger pull-right">{(!usernamePass && this.state.username.length > 0) ? this.i18n('register.username-not-matching-tooltip') : null}</span>
                        <span
                            className="error text-danger pull-right">{this.state.usernameExists ? this.i18n('profile.username.exists') : null}</span>
                    </div>


                    <div className='col-xs-6'>
                        <Input type='text' name='phoneNumber' label='Telefonní číslo *'
                               value={this.state.phoneNumber}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!phonenumbPass && this.state.phoneNumber.length > 0 ? 'error' : null}
                               title={!phonenumbPass ? this.i18n('register.phoneNumber-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right">{(!phonenumbPass && this.state.phoneNumber.length > 0) ? this.i18n('register.phoneNumber-not-matching-tooltip') : null}</span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <h4>Info</h4>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input type='text' name='degreeAfter' label='Titul za *'
                               value={this.state.degreeAfter}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!degreeAfter && this.state.degreeAfter.length > 0 ? 'error' : null}
                               title={!degreeAfter ? this.i18n('register.street-not-matching-tooltip') : null}/>
                        <span
                            className="error text-danger pull-right">{(!degreeAfter && this.state.degreeAfter.length > 0) ? this.i18n('register.street-not-matching-tooltip') : null}</span>
                    </div>
                    <div className='col-xs-6'>
                        <Input type='text' name='degreeBefore' label='Titul před *'
                               value={this.state.degreeBefore}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!degreeBefore && this.state.degreeBefore.length > 0 ? 'error' : null}
                               title={!degreeBefore ? this.i18n('register.street-not-matching-tooltip') : null}/>
                        <span
                            className="error text-danger pull-right">{(!degreeBefore && this.state.degreeBefore.length > 0) ? this.i18n('register.street-not-matching-tooltip') : null}</span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input type='text' name='description' label='Popis *'
                               value={this.state.description}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!description && this.state.description.length > 0 ? 'error' : null}
                               title={!description ? this.i18n('register.street-not-matching-tooltip') : null}/>
                        <span
                            className="error text-danger pull-right">{(!description && this.state.description.length > 0) ? this.i18n('register.street-not-matching-tooltip') : null}</span>
                    </div>
                    <div className='col-xs-6'>
                        <Input type='text' name='crn' label='IČO *'
                               value={this.state.crn}
                               labelWidth={4} inputWidth={8} onChange={this.onChange}
                               validation={!crn && this.state.crn.length > 0 ? 'error' : null}
                               title={!crn ? this.i18n('register.street-not-matching-tooltip') : null}/>
                        <span
                            className="error text-danger pull-right">{(!crn && this.state.crn.length > 0) ? this.i18n('register.street-not-matching-tooltip') : null}</span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <h4>Heslo</h4>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input type='password' name='password' label={this.i18n('register.password') + '*'}
                               labelWidth={4} inputWidth={8}
                               onChange={this.onChange} value={this.state.password}/>
                    </div>
                    <div className='col-xs-6'>
                        {this.renderPasswordConfirm()}
                    </div>
                </div>
                <div style={{margin: '1em 0em 0em 0em', textAlign: 'center'}}>
                    <Button bsStyle='success' bsSize='small' ref='submit'
                            disabled={!this.isValid() || this.state.mask || !usernamePass || !firstnamePass || !lastnamePass || !phonenumbPass || !degreeAfter || !degreeBefore || !description || !crn}
                            onClick={this._addDoctor}>{this.i18n('add')}</Button>
                </div>
            </form>;
    }

    renderAlert() {
        return this.state.alertVisible ?
            <Alert bsStyle='danger' bsSize='small' onDismiss={this.dismissAlert}>
                <div>{this.state.errorMessage}</div>
            </Alert> : null;
    }

    renderPasswordConfirm() {
        const passwordMatch = this.state.password === this.state.passwordConfirm;
        return <Input type='password' name='passwordConfirm' label={this.i18n('register.password-confirm') + '*'}
                      labelWidth={4} inputWidth={8} onChange={this.onChange} onKeyDown={this.onKeyDown}
                      value={this.state.passwordConfirm} validation={!passwordMatch ? 'error' : null}
                      title={!passwordMatch ? this.i18n('register.passwords-not-matching-tooltip') : null}/>;
    }

}

AddDoctor.propTypes = {
    actions: PropTypes.object.isRequired
};

export default injectIntl(I18nWrapper(AddDoctor));
