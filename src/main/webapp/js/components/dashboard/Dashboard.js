'use strict';

import React from "react";

import PropTypes from "prop-types";
import {Col, Grid, Jumbotron, Row} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Tile from "./DashboardTile";

class Dashboard extends React.Component {

    constructor(props) {
        super(props);
        this.i18n = props.i18n;
    }

    render() {
        return <div className='dashboard-container'>
            <div className='col-xs-8 top-buffer'>
                <Jumbotron>
                    {this.renderTitle()}
                </Jumbotron>
            </div>
            <div className='col-xs-4'>
                <div>
                    <img
                        alt="User Picture"
                        src="https://cms-assets.tutsplus.com/uploads/users/34/posts/30128/preview_image/reservations.jpg"
                        className="img-circle img-responsive"
                    />
                </div>
            </div>
        </div>;
    }

    renderTitle() {
        return <h3 className>
            <FormattedMessage
                id='dashboard.welcome'
                values={{
                    name: <span className='bold'>{this.props.userFirstName}</span>
                }}
            />
        </h3>;
    }

    _renderMainDashboard() {
        return <Grid fluid={true}>
            <Row>
                <Col xs={6} className='dashboard-sector'>
                    <Tile onClick={this.props.createEmptyReport}>{this.i18n('dashboard.create-tile')}</Tile>
                </Col>
                <Col xs={6} className='dashboard-sector'>
                    <Tile
                        onClick={this.props.showAllReports}>{this.i18n('dashboard.view-all-tile')}</Tile>
                </Col>
            </Row>
        </Grid>;
    }
}

Dashboard.propTypes = {
    createEmptyReport: PropTypes.func.isRequired,
    showAllReports: PropTypes.func.isRequired,
    openReport: PropTypes.func.isRequired,
    userFirstName: PropTypes.string,
};

export default injectIntl(I18nWrapper(Dashboard));
