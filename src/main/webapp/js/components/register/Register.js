'use strict';

import React from "react";
import {Alert, Button, Panel} from "react-bootstrap";
import Ajax from "../../utils/Ajax";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Input from "../HorizontalInput";
import Logger from "../../utils/Logger";
import Mask from "../Mask";
import PersonValidator from "../../validator/PersonValidator";
import Routing from "../../utils/Routing";
import Routes from "../../utils/Routes";
import Select from "react-select";

class Register extends React.Component {

    constructor(props) {
        super(props);
        this.i18n = props.i18n;
        this.state = {
            firstName: '',
            lastName: '',
            username: '',
            password: '',
            passwordConfirm: '',
            birthNumber: '',
            phoneNumber: '',
            city: '',
            street: '',
            houseNumber: '',
            zipCode: '',
            alertVisible: false,
            errorMessage: '',
            usernameExists: false,
            birthNumberExists: false,
            mask: false
        };
    }

    componentWillUnmount() {
        if (this.alertTimeout) {
            clearTimeout(this.alertTimeout);
        }
    }

    onChange = (e) => {
        const change = {};
        change[e.target.name] = e.target.value;
        this.setState(change);
    };

    _onUsernameChange = (e) => {
        this.onChange(e);
        const value = e.target.value;
        let username = document.getElementById("username");

        if (this.state.username !== e.target.value) {
            Ajax.get('rest/persons/exists?username=' + value).end((data) => {
                if (data === 'true') {
                    this.setState({usernameExists: true});
                    username.title = this.i18n('profile.username.exists');
                } else {
                    this.setState({usernameExists: false});
                    username.title = this.i18n('register.username-not-matching-tooltip');
                }
            });
        }
    };

    _onBirthNumberChange = (e) => {
        this.onChange(e);
        const value = e.target.value;
        let birthNumber = document.getElementById("birthNumber");
        if (this.state.birthNumber !== e.target.value) {
            Ajax.get('rest/patients/existsBirthNumber?birthNumber=' + value).end((data) => {
                if (data === 'true') {
                    this.setState({birthNumberExists: true});
                    birthNumber.title = this.i18n('profile.birthNumber.exists');
                } else {
                    this.setState({birthNumberExists: false});
                    birthNumber.title = this.i18n('register.birthnumber-not-matching-tooltip');
                }
            });
        }
        console.log(this.state);
    };

    onKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.register();
        }
    };

    showAlert = (msg) => {
        this.setState({alertVisible: true, errorMessage: msg, mask: false});
        this.alertTimeout = setTimeout(this.dismissAlert, 5000);
    };

    dismissAlert = () => {
        this.setState({alertVisible: false});
        this.alertTimeout = null;
    };

    isValid = () => {
        return PersonValidator.isValid(this.state);
    };

    register = () => {
        if (!this.isValid()) {
            return;
        }
        const userData = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.username,
            password: this.state.password,
            role: '1',
            phoneNumber: this.state.phoneNumber,
            birthNumber: this.state.birthNumber,
            address: {
                city: this.state.city,
                street: this.state.street,
                houseNumber: this.state.houseNumber,
                zipCode: this.state.zipCode
            }
        };
        Ajax.post('rest/persons', userData).end(function (body, resp) {
            if (resp.status === 201) {
                this.doSyntheticLogin(userData.email, userData.password);
            }
        }.bind(this), function (err) {
            this.showAlert(err.message ? err.message : 'Unknown error.');
        }.bind(this));
        this.setState({mask: true});
    };

    /**
     * After successful registration, perform a synthetic login so that the user receives his session and can start
     * working.
     */
    doSyntheticLogin = (username, password) => {
        Ajax.post('j_spring_security_check', null, 'form').send('username=' + username).send('password=' + password)
            .end(function (data, resp) {
                const status = JSON.parse(resp.text);
                if (!status.success || !status.loggedIn) {
                    this.showAlert();
                    return;
                }
                Routing.transitionToHome();
            }.bind(this), function (err) {
                Logger.error('Unable to perform synthetic login. Received response with status ' + err.status);
            });
    };

    cancel = () => {
        Routing.transitionTo(Routes.login);
    };


    render() {

        let options = [];

        const panelCls = this.state.alertVisible ? 'register-panel expanded' : 'register-panel',
            mask = this.state.mask ? (<Mask text={this.i18n('register.mask')}/>) : null;

        const numberRegex = /^[0-9]+$/;
        const nameRegex = /^[a-zěščřžýáíéňA-ZĚŠČŘŽÝÁÍÉŇ]+$/;
        const streetRegex = /^[a-zěščřžýáíéňA-ZĚŠČŘŽÝÁÍÉŇ0-9 ]+$/;
        const usernameRegex = /^[a-zA-Z0-9]+$/;
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        const firstnamePass = this.state.firstName.length <= 30 && this.state.firstName.match(nameRegex);
        const lastnamePass = this.state.lastName.length <= 30 && this.state.lastName.match(nameRegex);
        const usernamePass = this.state.username.length <= 40 && this.state.username.match(emailRegex);
        const phonenumbPass = this.state.phoneNumber.length == 9 && this.state.phoneNumber.match(numberRegex);
        const birthNumberPass = this.state.birthNumber.length == 10 && this.state.birthNumber.match(numberRegex);

        const streetPass = this.state.street.length <= 30 && this.state.street.match(streetRegex);
        const housenumPass = this.state.houseNumber.length <= 10 && this.state.houseNumber.match(numberRegex);
        const zipcodePass = this.state.zipCode.length == 5 && this.state.zipCode.match(numberRegex);
        const cityPass = this.state.city.length <= 30 && this.state.city.match(nameRegex);

        return <Panel header={<h3>{this.i18n('register.title')}</h3>} bsStyle='info' className={panelCls}>
            {mask}
            <form className='form-horizontal' style={{margin: '0.5em 0 0 0'}}>
                {this.renderAlert()}
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='firstName' label={'First name *'}
                            value={this.state.firstName}
                            labelWidth={4} inputWidth={8} onChange={this.onChange}
                            validation={!firstnamePass && this.state.firstName.length > 0 ? 'error' : null}
                            title={!firstnamePass ? this.i18n('register.firstName-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!firstnamePass && this.state.firstName.length > 0) ? this.i18n('register.firstName-not-matching-tooltip') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='lastName' label={'Last name *'}
                            value={this.state.lastName}
                            labelWidth={4} inputWidth={8} onChange={this.onChange}
                            validation={!lastnamePass && this.state.lastName.length > 0 ? 'error' : null}
                            title={!lastnamePass ? this.i18n('register.lastName-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!lastnamePass && this.state.lastName.length > 0) ? this.i18n('register.lastName-not-matching-tooltip') : null}
                        </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='username' label={'Email *'}
                            title={!usernamePass ? this.i18n('register.username-not-matching-tooltip') : null}
                            value={this.state.username}
                            validation={this.state.usernameExists || !usernamePass && this.state.username.length > 0 ? 'error' : null}
                            labelWidth={4} inputWidth={8} onChange={this._onUsernameChange}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!usernamePass && this.state.username.length > 0) ? this.i18n('register.username-not-matching-tooltip') : null}
                        </span>
                        <span
                            className="error text-danger pull-right"
                        >
                            {this.state.usernameExists ? this.i18n('profile.username.exists') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='phoneNumber' label='Phone number *'
                            value={this.state.phoneNumber}
                            labelWidth={4} inputWidth={8} onChange={this.onChange}
                            validation={!phonenumbPass && this.state.phoneNumber.length > 0 ? 'error' : null}
                            title={!phonenumbPass ? this.i18n('register.phoneNumber-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!phonenumbPass && this.state.phoneNumber.length > 0) ? 'Phone number is 9 digits long number' : null}
                        </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='birthNumber' label='ID number *'
                            title={!birthNumberPass ? this.i18n('register.birthnumber-not-matching-tooltip') : null}
                            value={this.state.birthNumber}
                            validation={this.state.birthNumberExists || !birthNumberPass && this.state.birthNumber.length > 0 ? 'error' : null}
                            labelWidth={4} inputWidth={8} onChange={this._onBirthNumberChange}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!birthNumberPass && this.state.birthNumber.length > 0) ? 'ID number is 10 digits long number' : null}
                        </span>
                        <span
                            className="error text-danger pull-right"
                        >
                            {this.state.birthNumberExists ? 'This ID already exists' : null}
                        </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <h4>Adresa</h4>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='street' label='Street *'
                            value={this.state.street}
                            labelWidth={4} inputWidth={8} onChange={this.onChange}
                            validation={!streetPass && this.state.street.length > 0 ? 'error' : null}
                            title={!streetPass ? this.i18n('register.street-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!streetPass && this.state.street.length > 0) ? this.i18n('register.street-not-matching-tooltip') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='houseNumber' label='House number *'
                            value={this.state.houseNumber}
                            labelWidth={4} inputWidth={8} onChange={this.onChange}
                            validation={!housenumPass && this.state.houseNumber.length > 0 ? 'error' : null}
                            title={!housenumPass ? this.i18n('register.houseNumber-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!housenumPass && this.state.houseNumber.length > 0) ? this.i18n('register.houseNumber-not-matching-tooltip') : null}
                        </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='city' label='City *'
                            value={this.state.city}
                            labelWidth={4} inputWidth={8} onChange={this.onChange}
                            validation={!cityPass && this.state.city.length > 0 ? 'error' : null}
                            title={!streetPass ? this.i18n('register.city-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!cityPass && this.state.city.length > 0) ? this.i18n('register.city-not-matching-tooltip') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='zipCode' label='ZIP code *'
                            value={this.state.zipCode}
                            labelWidth={4} inputWidht={8} onChange={this.onChange}
                            validation={!zipcodePass && this.state.zipCode.length > 0 ? 'error' : null}
                            title={!zipcodePass ? this.i18n('register.zipCode-not-matching-tooltip') : null}
                        />
                        <span
                            className="error text-danger pull-right"
                        >
                            {(!zipcodePass && this.state.zipCode.length > 0) ? this.i18n('register.zipCode-not-matching-tooltip') : null}
                            </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <h4>Heslo</h4>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='password' name='password' label={'Password *'}
                            labelWidth={4} inputWidth={8}
                            onChange={this.onChange} value={this.state.password}
                        />
                    </div>
                    <div className='col-xs-6'>
                        {this.renderPasswordConfirm()}
                    </div>
                </div>
                <div style={{margin: '1em 0em 0em 0em', textAlign: 'center'}}>
                    <Button
                        bsStyle='success' bsSize='small' ref='submit'
                        disabled={!this.isValid() || this.state.mask || !usernamePass || !firstnamePass || !lastnamePass || !phonenumbPass || !birthNumberPass || !streetPass || !housenumPass || !zipcodePass || !cityPass}
                        onClick={this.register}
                    >
                        {this.i18n('register.submit')}
                        </Button>
                    <Button
                        bsSize='small' onClick={this.cancel} style={{margin: '0 0 0 3.2em'}}
                        disabled={this.state.mask}
                    >
                        {this.i18n('cancel')}
                    </Button>
                </div>
            </form>
        </Panel>;
    }

    renderAlert() {
        return this.state.alertVisible ?
            <Alert bsStyle='danger' bsSize='small' onDismiss={this.dismissAlert}>
                <div>{this.state.errorMessage}</div>
            </Alert> : null;
    }

    renderPasswordConfirm() {
        const passwordMatch = this.state.password === this.state.passwordConfirm;
        return <Input type='password' name='passwordConfirm' label={'Password confirm *'}
                      labelWidth={4} inputWidth={8} onChange={this.onChange} onKeyDown={this.onKeyDown}
                      value={this.state.passwordConfirm} validation={!passwordMatch ? 'error' : null}
                      title={!passwordMatch ? this.i18n('register.passwords-not-matching-tooltip') : null}/>;
    }
}

export default injectIntl(I18nWrapper(Register));
