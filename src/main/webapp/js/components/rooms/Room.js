'use strict';

import React from "react";

import PropTypes from "prop-types";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Mask from "../Mask";
import ReactTable from "react-table";

class Room extends React.Component {

    constructor(props) {
        super(props);
        this.i18n = props.i18n;
    }

    _checkReservation(row, time) {
        row = row.original;
        for (let k = 0; k < row.reservations.length; k++) {
            if (row.reservations[k].timeFrom === time) {
                return <div style={{ textAlign: "center" }}>X</div>
            }
        }

        return <div style={{ textAlign: "center" }}>-</div>;
    }

    render() {
        if (!this.props.dayReservations && !this.props.room) {
            return <Mask text='Loading...'/>;
        }

        return <div>
            <ReactTable
                data={this.props.dayReservations}
                noDataText="No data to show."
                columns={[
                    {
                        Header: "Room reservations",
                        columns: [
                            {
                                Header: "Date",
                                id: "date",
                                accessor: d => d.date,
                            },
                            {
                                Header: "9:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "09:00"),
                            },
                            {
                                Header: "9:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "09:30"),
                            },
                            {
                                Header: "10:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "10:00"),
                            },
                            {
                                Header: "10:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "10:30"),
                            },
                            {
                                Header: "11:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "11:00"),
                            },
                            {
                                Header: "11:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "11:30"),
                            },
                            {
                                Header: "12:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "12:00"),
                            },
                            {
                                Header: "12:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "12:30"),
                            },
                            {
                                Header: "13:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "13:00"),
                            },
                            {
                                Header: "13:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "13:30"),
                            },
                            {
                                Header: "14:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "14:00"),
                            },
                            {
                                Header: "14:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "14:30"),
                            },
                            {
                                Header: "15:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "15:00"),
                            },
                            {
                                Header: "15:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "15:30"),
                            },
                            {
                                Header: "16:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "16:00"),
                            },
                            {
                                Header: "16:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "16:30"),
                            },
                            {
                                Header: "17:00",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "17:00"),
                            },
                            {
                                Header: "17:30",
                                accessor: "id",
                                Cell: d => this._checkReservation(d, "17:30"),
                            }
                        ]
                    }
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
            />
            <br />
        </div>;
    }

}

Room.propTypes = {
    room: PropTypes.object,
    dayReservations: PropTypes.array,
    actions: PropTypes.object.isRequired
};

export default injectIntl(I18nWrapper(Room));
