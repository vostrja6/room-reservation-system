'use strict';

import React from "react";
import {Button} from "react-bootstrap";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Mask from "../Mask";
import PropTypes from "prop-types";
import ReactTable from 'react-table'

class Rooms extends React.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
    }

    render() {
        if (!this.props.rooms) {
            return <Mask text='Loading...'/>;
        }
        console.log(this.props.rooms);
        return <div>
            <ReactTable
                data={this.props.rooms}
                noDataText="No data to show."
                columns={[
                    {
                        Header: "Room",
                        columns: [
                            {
                                Header: "Building",
                                id: "building",
                                accessor: d => d.building,
                            },
                            {
                                Header: "Block",
                                id: "block",
                                accessor: d => d.block,
                            },
                            {
                                Header: "Number",
                                id: "number",
                                accessor: d => d.number,
                            }
                        ]
                    },
                    {
                        Header: "Info",
                        columns: [
                            {
                                accessor: "id",
                                Cell: row => (this._renderButtons(row.value))
                            },
                        ]
                    },
                ]}
                defaultPageSize={10}
                className="-striped -highlight"
            />
            <br />
        </div>;
    }

    _renderButtons(roomId)
    {
        return <div>
            <Button
                bsStyle='primary'
                onClick={() => this.props.actions.showRoomDetail(roomId)}
            >
                Show
            </Button>
        </div>;
    }
}

Rooms.propTypes = {
    rooms: PropTypes.array,
    actions: PropTypes.object.isRequired
};

export default injectIntl(I18nWrapper(Rooms));
