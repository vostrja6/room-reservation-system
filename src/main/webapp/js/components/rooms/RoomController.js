'use strict';

import React from "react";
import Reflux from "reflux";

import Actions from "../../actions/Actions";
import injectIntl from "../../utils/injectIntl";
import I18nWrapper from "../../i18n/I18nWrapper";
import LoadingWrapper from "../misc/LoadingWrapper";
import MessageWrapper from "../misc/MessageWrapper";
import ResourceNotFound from "../ResourceNotFound";
import UserStore from "../../stores/UserStore";
import RoomStore from "../../stores/RoomStore";
import Room from "./Room";

class RoomController extends Reflux.Component {

    constructor(props) {
        super(props);
        this.i18n = props.i18n;
        this.state = {
            loading: false,
            day_reservations: [],
        };
        this.stores = [RoomStore, UserStore];
        this.storeKeys = ['room'];
    }

    componentWillMount() {
        super.componentWillMount();
        this._loadRoom();
    }

    _loadRoom() {
        this.setState({loading: true});
        Actions.loadRoom(this.props.match.params.roomId, this._onLoadFinish);
        this.props.loadingOn();
    }

    _onLoadFinish = () => {
        this.props.loadingOff();
        this.setState({loading: false});
    };

    getDayRoomReservations(roomId, date) {
        console.log('get days room reservations');
        Actions.loadRoomDayReservations(roomId, date, status);
    }

    onSetDayRoomReservations() {
        if (!this.state.room) {
            return;
        }

        var days = [];
        var dayz = [];
        let x = 0;

        console.log("Room reserv");
        console.log(this.state.room.reservations);

        for(let i = 0; i < this.state.room.reservations.length; i++) {
            if (!days.includes(this.state.room.reservations[i].date)) {
                days[x] = this.state.room.reservations[i].date;
                x++;
            }
        }

        var date_now = new Date();



        for(let i = 0; i < days.length; i++) {
            var res_date = parseInt(days[i].substring(0, 2));
            var res_month = parseInt(days[i].substring(3, 5));
            var res_year = parseInt('20' + days[i].substring(6, 8));

            var date_res = new Date(res_year, (res_month-1), res_date);

            var diff = parseInt((date_res-date_now)/(60000*60*24));

            console.log("Reservation date:");
            console.log(date_res);
            console.log("Time difference:");
            console.log(diff);

            if (diff >= 0) {
                console.log("adding");
                dayz.push(days[i]);
            }
        }

        days = dayz;

        if (!days) {
            console.log("no upcoming days reserved");
            return;
        }

        console.log(days);

        var day = new Array(days.length)

        if (!(days.length === 0)) {
            console.log("loop");
            for (let k = 0; k < days.length; k++) {
                day[k] = {id: '', date: '', reservations: []};
                var res_array = [];
                for (let j = 0; j < this.state.room.reservations.length; j++) {
                    if (days[k] === this.state.room.reservations[j].date) {
                        res_array.push(this.state.room.reservations[j]);
                    }
                }
                day[k].id = k;
                day[k].date = days[k];
                day[k].reservations = res_array;
            }

        }
        console.log("days reservations set:");
        this.state.day_reservations = day;
        console.log(this.state.day_reservations);
        return this.state.day_reservations;
    }

    render() {
        const room = this.state.room;
        // const actions = {
        //     getDayRoomReservations: this.onGetDayRoomReservations,
        // };
        const actions = {};


        if (!this.state.loading && !room) {
            return <ResourceNotFound identifier={this.props.match.params.roomId}
                                     resource='Room'/>;
        }

        this.onSetDayRoomReservations();
        const dayReservations = this.state.day_reservations;

        if (!this.state.day_reservations) {
            return;
        }
        return <Room room={room} dayReservations={dayReservations} actions={actions}/>;
    }
}

export default injectIntl(I18nWrapper(MessageWrapper(LoadingWrapper(RoomController))));