'use strict';

import React from "react";
import Reflux from "reflux";
import PropTypes from "prop-types";
import JsonLdUtils from "jsonld-utils";

import Actions from "../../actions/Actions";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import MessageWrapper from "../misc/MessageWrapper";
import RoomStore from  "../../stores/RoomStore";
import Routes from "../../utils/Routes";
import Routing from "../../utils/Routing";
import RoutingRules from "../../utils/RoutingRules";
import Rooms from './Rooms'

class RoomsController extends Reflux.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;
        this.state = {
            loading: false
        };
        this.stores = [RoomStore];
        this.storeKeys = ['rooms'];
    }

    componentWillMount() {
        super.componentWillMount();
        RoutingRules.execute(Routes.rooms.name);
        this.setState({rooms: null});
        Actions.loadAllRooms()
    }

    onShowRoomDetail = (roomId) => {
        Routing.transitionTo(Routes.room, {
            params: {roomId: roomId}
        });
    };

    render() {
        const actions = {
            showRoomDetail: this.onShowRoomDetail
        };
        return <Rooms
            rooms={this.state.rooms}
            actions={actions}
        />;
    }
}


export default injectIntl(I18nWrapper(MessageWrapper(RoomsController)));
