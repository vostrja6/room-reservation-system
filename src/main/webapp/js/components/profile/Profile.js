import React from "react";
import PropTypes from "prop-types";
import {Button, Panel} from "react-bootstrap";

import Ajax from "../../utils/Ajax";
import I18nWrapper from "../../i18n/I18nWrapper";
import injectIntl from "../../utils/injectIntl";
import Input from "../HorizontalInput";
import LoadingWrapper from "../misc/LoadingWrapper";
import MessageWrapper from "../misc/MessageWrapper";
import PersonValidator from "../../validator/PersonValidator";

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.i18n = props.i18n;

        this.state = {
            passwordEdit: false,
            usernameExists: false,
            originalUsername: props.user.username,

            id: this.props.user.id,
            firstName: this.props.user.firstName,
            lastName: this.props.user.lastName,
            username: this.props.user.username,
            email: this.props.user.username,
            birthNumber: this.props.user.birthNumber,
            phoneNumber: this.props.user.phoneNumber,
            city: this.props.user.address.city,
            street: this.props.user.address.street,
            houseNumber: this.props.user.address.houseNumber,
            address: {
                city: this.props.user.address.city,
                street: this.props.user.address.street,
                houseNumber: this.props.user.address.houseNumber,
                zipCode: this.props.user.address.zipCode
            },
            zipCode: this.props.user.address.zipCode,
            alertVisible: false,
            errorMessage: '',
        }
    }

    _onSave = (userData) => {
        this.props.loadingOn();
        this.props.onSave(userData, this._onSaveSuccess, this._onSaveError);
    };

    _onSaveSuccess = () => {
        this.setState({originalUsername: this.props.user.username});
        this.props.loadingOff();
        this.props.showSuccessMessage(this.i18n('profile.update.success'));
    };

    _onSaveError = (error) => {
        this.props.loadingOff();
        this.props.showErrorMessage(this.i18n('profile.update.error') + error.message);
    };

    _onChange = (e) => {
        const change = {};
        change[e.target.name] = e.target.value;
        this.props.onChange(change);
        this.setState(change);
    };

    _onUsernameChange = (e) => {
        this._onChange(e);
        const value = e.target.value;
        if (this.state.originalUsername !== e.target.value) {
            Ajax.get('rest/persons/exists?username=' + value).end((data) => {
                if (data === 'true') {
                    this.setState({usernameExists: true});
                } else {
                    this.setState({usernameExists: false});
                }
            });
        }
    };

    _togglePasswordEdit = () => {
        const change = {
            password: null,
            passwordConfirm: null,
            passwordOriginal: null
        };
        this.props.onChange(change);
        this.setState({passwordEdit: !this.state.passwordEdit});
    };

    _onPreSave = () =>
    {
        const userData = {
            id: this.state.id,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.username,
            passwordOriginal: this.props.user.passwordOriginal,
            password: this.state.password,
            phoneNumber: this.state.phoneNumber,
            birthNumber: this.state.birthNumber,
            address: {
                city: this.state.city,
                street: this.state.street,
                houseNumber: this.state.houseNumber,
                zipCode: this.state.zipCode
            }
        };
        this._onSave(userData);
    };

    render() {
        const user = this.props.user,
            isValid = PersonValidator.isValid(user, this.state.passwordEdit),
            passwordsMatch = user.password === user.passwordConfirm;


        var numberRegex = /^[0-9]+$/;
        var nameRegex = /^[a-zěščřžýáíéňA-ZĚŠČŘŽÝÁÍÉŇ]+$/;
        var streetRegex = /^[a-zěščřžýáíéňA-ZĚŠČŘŽÝÁÍÉŇ0-9 ]+$/;
        var usernameRegex = /^[a-zA-Z0-9]+$/;
        var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        const firstnamePass = this.state.firstName.length <= 30 && this.state.firstName.match(nameRegex);
        const lastnamePass = this.state.lastName.length <= 30 && this.state.lastName.match(nameRegex);
        const usernamePass = this.state.username.length <= 40 && this.state.username.match(emailRegex);
        const phonenumbPass = this.state.phoneNumber.length == 9 && this.state.phoneNumber.match(numberRegex);
        const birthNumberPass = this.state.birthNumber.length == 10 && this.state.birthNumber.match(numberRegex);

        const streetPass = this.state.street.length <= 30 && this.state.street.match(streetRegex);
        const housenumPass = this.state.houseNumber.length <= 10 && this.state.houseNumber.match(numberRegex);
        const zipcodePass = this.state.zipCode.length == 5 && this.state.zipCode.match(numberRegex);
        const cityPass = this.state.city.length <= 30 && this.state.city.match(nameRegex);


        return <Panel header={<h5>{this.i18n('profile.header')}</h5>} bsStyle='info' className='profile-panel'>
            <form className='form-horizontal profile-form'>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='firstName' label={this.i18n('register.firstName') + '*'}
                            value={this.state.firstName}
                            labelWidth={4} inputWidth={8} onChange={this._onChange}
                            validation={!firstnamePass && this.state.firstName.length > 0 ? 'error' : null}
                            title={!firstnamePass ? this.i18n('register.firstName-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!firstnamePass && this.state.firstName.length > 0) ? this.i18n('register.firstName-not-matching-tooltip') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='lastName' label={this.i18n('register.lastName') + '*'}
                            value={this.state.lastName}
                            labelWidth={4} inputWidth={8} onChange={this._onChange}
                            validation={!lastnamePass && this.state.lastName.length>0 ? 'error' : null}
                            title={!lastnamePass ? this.i18n('register.lastName-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!lastnamePass && this.state.lastName.length > 0) ? this.i18n('register.lastName-not-matching-tooltip') : null}
                            </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='username' label={this.i18n('register.username') + '*'}
                            title={!usernamePass ? this.i18n('register.username-not-matching-tooltip') : null}
                            value={this.state.username} validation={this.state.usernameExists || !usernamePass && this.state.username.length>0 ? 'error' : null}
                            labelWidth={4} inputWidth={8} onChange={this._onUsernameChange}
                        />
                        <span className = "error text-danger pull-right">
                            {(!usernamePass && this.state.username.length > 0) ? this.i18n('register.username-not-matching-tooltip') : null}
                            </span>
                        <span className = "error text-danger pull-right">
                            {this.state.usernameExists ? this.i18n('profile.username.exists') : null}
                            </span>
                    </div>

                    <div className='col-xs-6'>
                        <Input
                            type='text' name='phoneNumber' label='Telefonní číslo *'
                            value={this.state.phoneNumber}
                            labelWidth={4} inputWidth={8} onChange={this._onChange}
                            validation={!phonenumbPass && this.state.phoneNumber.length > 0 ? 'error' : null}
                            title={!phonenumbPass ? this.i18n('register.phoneNumber-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!phonenumbPass && this.state.phoneNumber.length > 0) ? this.i18n('register.phoneNumber-not-matching-tooltip') : null}
                        </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='birthNumber' label='Rodné číslo*'
                            title={!birthNumberPass ? this.i18n('register.birthnumber-not-matching-tooltip') : null}
                            value={this.state.birthNumber} validation={this.state.birthNumberExists || !birthNumberPass && this.state.birthNumber.length > 0 ? 'error' : null}
                            labelWidth={4} inputWidth={8} onChange={this._onChange} disabled = "true"
                        />
                        <span className = "error text-danger pull-right">
                            {(!birthNumberPass && this.state.birthNumber.length > 0) ? this.i18n('register.birthnumber-not-matching-tooltip') : null}
                            </span>
                        <span className = "error text-danger pull-right">
                            {this.state.birthNumberExists ? this.i18n('profile.birthNumber.exists') : null}
                            </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <h4>Adresa</h4>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='street' label='Ulice *'
                            value={this.state.street}
                            labelWidth={4} inputWidth={8} onChange={this._onChange}
                            validation={!streetPass && this.state.street.length > 0 ? 'error' : null}
                            title={!streetPass ? this.i18n('register.street-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!streetPass && this.state.street.length > 0) ? this.i18n('register.street-not-matching-tooltip') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='houseNumber' label='Číslo popisné *'
                            value={this.state.houseNumber}
                            labelWidth={4} inputWidth={8} onChange={this._onChange}
                            validation={!housenumPass && this.state.houseNumber.length > 0 ? 'error' : null}
                            title={!housenumPass ? this.i18n('register.houseNumber-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!housenumPass && this.state.houseNumber.length > 0) ? this.i18n('register.houseNumber-not-matching-tooltip') : null}
                        </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='city' label='Město *'
                            value={this.state.city}
                            labelWidth={4} inputWidth={8} onChange={this._onChange}
                            validation={!cityPass && this.state.city.length > 0 ? 'error' : null}
                            title={!streetPass ? this.i18n('register.city-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!cityPass && this.state.city.length > 0) ? this.i18n('register.city-not-matching-tooltip') : null}
                        </span>
                    </div>
                    <div className='col-xs-6'>
                        <Input
                            type='text' name='zipCode' label='PSČ *'
                            value={this.state.zipCode}
                            labelWidth={4} inputWidht={8} onChange={this._onChange}
                            validation={!zipcodePass && this.state.zipCode.length > 0 ? 'error' : null}
                            title={!zipcodePass ? this.i18n('register.zipCode-not-matching-tooltip') : null}
                        />
                        <span className = "error text-danger pull-right">
                            {(!zipcodePass && this.state.zipCode.length > 0) ? this.i18n('register.zipCode-not-matching-tooltip') : null}
                            </span>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-xs-1'>
                        <Button
                            bsSize='small' bsStyle='link'
                            onClick={this._togglePasswordEdit}
                        >
                            {this.i18n('profile.password.toggle')}
                        </Button>
                    </div>
                </div>
                {this._renderPasswordEdit()}
            </form>
            <div className='row'>
                <div className='col-xs-6'>
                    <div className='float-right'>
                        <Button
                            onClick={this._onPreSave} bsSize='small' bsStyle='success'
                            title={!isValid ? this.i18n(passwordsMatch ? 'profile.invalid' : 'register.passwords-not-matching-tooltip') : null}
                            disabled={!isValid ||
                            !usernamePass ||
                            !firstnamePass ||
                            !lastnamePass ||
                            !phonenumbPass ||
                            !birthNumberPass ||
                            !streetPass ||
                            !housenumPass ||
                            !zipcodePass ||
                            !cityPass}
                        >
                            {this.i18n('save')}
                        </Button>
                    </div>
                </div>
                <div className='col-xs-6'>
                    <Button onClick={this.props.onClose} bsSize='small' bsStyle='link'>{this.i18n('close')}</Button>
                </div>
            </div>
        </Panel>;
    }

    _renderPasswordEdit() {
        if (!this.state.passwordEdit) {
            return null;
        }
        const user = this.props.user,
            passwordsMatch = user.password === user.passwordConfirm;
        return <div>
            <div className='row'>
                <div className='col-xs-6'>
                    <Input
                        type='password' name='passwordOriginal' label={this.i18n('profile.password.original') + '*'}
                        labelWidth={4} inputWidth={8}
                        onChange={this._onChange} value={user.passwordOriginal ? user.passwordOriginal : ''}
                    />
                </div>
            </div>
            <div className='row'>
                <div className='col-xs-6'>
                    <Input
                        type='password' name='password' label={this.i18n('profile.password.new') + '*'}
                        labelWidth={4} inputWidth={8}
                        onChange={this._onChange} value={user.password ? user.password : ''}
                    />
                </div>
                <div className='col-xs-6'>
                    <Input
                        type='password' name='passwordConfirm' label={this.i18n('profile.password.confirm') + '*'}
                        title={this.i18n(passwordsMatch ? null : 'register.passwords-not-matching-tooltip')}
                        labelWidth={4} inputWidth={8} validation={passwordsMatch ? null : 'error'}
                        onChange={this._onChange} value={user.passwordConfirm ? user.passwordConfirm : ''}
                    />
                </div>
            </div>
        </div>;
    }
}

Profile.propTypes = {
    user: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
};

export default injectIntl(I18nWrapper(LoadingWrapper(MessageWrapper(Profile))));
