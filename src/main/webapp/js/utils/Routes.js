'use strict';

export default {

    login: {name: 'login', path: '/login'},
    register: {name: 'register', path: '/register'},
    dashboard: {name: 'dashboard', path: '/dashboard'},
    profile: {name: 'profile', path: '/profile'},

    myReservations: {name: 'myReservations', path: '/myReservations'},
    reservation: {name: 'reservation', path: '/reservation'},
    rooms: {name: 'rooms', path: '/rooms'},
    room: {name: 'room', path: '/room/:roomId'},

}
