'use strict';

import createHistory from "history/createHashHistory";

import Constants from "../constants/Constants";
import RouterStore from "../stores/RouterStore";
import RoutingRules from "./RoutingRules";

class Routing {
    constructor() {
        this.history = createHistory();
        this.originalTarget = null;
    }

    /**
     * Transitions to the specified route
     * @param route Route object
     * @param options Transition options, can specify path parameters, query parameters, payload and view handlers.
     */
    transitionTo = (route, options) => {
        let path = route.path;
        if (!options) {
            options = {};
        }
        if (options.params) {
            path = Routing._setPathParams(path, options.params);
        }
        RouterStore.setTransitionPayload(route.name, options.payload);
        RoutingRules.execute(route.name);
        this.history.push(path);
    };

    static _setPathParams(path, params) {
        for (const paramName in params) {
            if (params.hasOwnProperty(paramName)) {
                path = path.replace(':' + paramName, params[paramName]);
            }
        }
        return path;
    }

    transitionToHome = (options) => {
        this.transitionTo(Constants.HOME_ROUTE, options);
    };

    saveOriginalTarget = (route) => {
        if (!route || route === '') {
            return;
        }
        this.originalTarget = route;
    };

    transitionToOriginalTarget = () => {
        if (this.originalTarget && this.originalTarget.path) {
            this.transitionTo(this.originalTarget);
        } else {
            this.transitionTo(Constants.HOME_ROUTE);
        }
    }
}

const INSTANCE = new Routing();

export default INSTANCE;
