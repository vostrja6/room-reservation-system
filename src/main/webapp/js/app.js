'use strict';

import I18nStore from "./stores/I18nStore";
import {addLocaleData, IntlProvider} from "react-intl";
import React from "react";
import ReactDOM from "react-dom";
import {Route, Router, Switch} from "react-router-dom";
import Routes from "./utils/Routes";
import Routing from "./utils/Routing";

import Login from "./components/login/Login";
import Register from "./components/register/Register";
import MainView from "./components/MainView";

let intlData = null;

function selectLocalization() {
    // Load react-intl locales
    if ('ReactIntlLocaleData' in window) {
        Object.keys(ReactIntlLocaleData).forEach(function (lang) {
            addLocaleData(ReactIntlLocaleData[lang]);
        });
    }
    const lang = navigator.language;
    if (lang && lang === 'cs' || lang === 'cs-CZ' || lang === 'sk' || lang === 'sk-SK') {
        intlData = require('./i18n/en');
    } else {
        intlData = require('./i18n/en');
    }
}

selectLocalization();
I18nStore.setMessages(intlData.messages);

// Wrapping router in a React component to allow Intl to initialize
const App = () => {
    return <IntlProvider {...intlData}>
        <Router history={Routing.history}>
            <Switch>
                <Route path={Routes.login.path} component={Login}/>
                <Route path={Routes.register.path} component={Register}/>
                <Route path='/' component={MainView}/>
            </Switch>
        </Router>
    </IntlProvider>;
};



ReactDOM.render(<App/>, document.getElementById("content"));
