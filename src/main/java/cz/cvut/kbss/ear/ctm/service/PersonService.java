package cz.cvut.kbss.ear.ctm.service;

import cz.cvut.kbss.ear.ctm.dao.GenericDao;
import cz.cvut.kbss.ear.ctm.dao.PersonDao;
import cz.cvut.kbss.ear.ctm.exception.AlreadyAddedReservationException;
import cz.cvut.kbss.ear.ctm.exception.AlreadyRemovedReservationException;
import cz.cvut.kbss.ear.ctm.exception.AuthorizationException;
import cz.cvut.kbss.ear.ctm.model.Address;
import cz.cvut.kbss.ear.ctm.model.Person;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.model.enums.Role;
import cz.cvut.kbss.ear.ctm.service.security.SecurityUtils;
import cz.cvut.kbss.ear.ctm.service.security.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonService extends AbstractRepositoryService<Person>
{
    private final PersonDao personDao;

    private final AddressService addressService;

    private final PasswordEncoder passwordEncoder;

    private final SecurityUtils securityUtils;

    private final UserDetailsService userDetailsService;


    @Autowired
    public PersonService(PersonDao personDao, AddressService addressService, PasswordEncoder passwordEncoder, SecurityUtils securityUtils, UserDetailsService userDetailsService) {
        this.personDao = personDao;
        this.addressService = addressService;
        this.passwordEncoder = passwordEncoder;
        this.securityUtils = securityUtils;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected GenericDao<Person> getPrimaryDao()
    {
        return personDao;
    }

    public Person findByEmail(String email)
    {
        return personDao.findByEmail(email);
    }

    @Transactional
    public void updatePerson(Person person, String originalPassword) {
        userDetailsService.verifyUniqueUsername(person);
        final Person current = securityUtils.getCurrentUser();
        if (!current.getId().equals(person.getId())) {
            throw new AuthorizationException("Modifying other user\'s account is forbidden.");
        }

        if (person.getPassword() != null) {
            securityUtils.verifyCurrentUserPassword(originalPassword);
            person.encodePassword(passwordEncoder);
        }
        else {
            person.setPassword(current.getPassword());
        }

        Address address = addressService.find(person.getAddress());
        if (address != null) {
            person.setAddress(address);
        }

        person.setReservations(this.find(person.getId()).getReservations());
        person.setRole(Role.USER);
        this.update(person);
        securityUtils.updateCurrentUser(person.getUsername());
    }

    @Transactional
    public void createPerson(Person person)
    {
        Address address = addressService.find(person.getAddress());
        person.setRole(Role.USER);
        if (address != null)
        {
            person.setAddress(address);
        }
        person.encodePassword(passwordEncoder);
        this.persist(person);
    }

    public Person findByBirthNumber(String birthNumber)
    {
        return personDao.findByBirthNumber(birthNumber);
    }
}
