package cz.cvut.kbss.ear.ctm.rest.mapper;

import cz.cvut.kbss.ear.ctm.dto.PersonDto;
import cz.cvut.kbss.ear.ctm.dto.ReservationDto;
import cz.cvut.kbss.ear.ctm.model.Person;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class DtoMapper {
    public abstract Person personDtoToPerson(PersonDto dto);

    public abstract Reservation reservationDtoToReservation(ReservationDto dto);
}
