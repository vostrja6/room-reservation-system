package cz.cvut.kbss.ear.ctm.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@NamedQueries({@NamedQuery(name = Room.ROOM_RESERVATIONS_PER_DATE,
        query = "select res from Reservation res left join Room  roo on roo.id=res.roomReservation.id where res.date=:date")})
public class Room extends AbstractEntity
{
    public static final String ROOM_RESERVATIONS_PER_DATE = "Room.findRoomReservationPerDate";

    @Basic(optional = false)
    @Column(nullable = false, length = 30, name = "name")
    private String name;

    @Basic(optional = false)
    @Column(nullable = false, length = 5, name = "building")
    private String building;

    @Basic(optional = false)
    @Column(nullable = false, length = 5, name = "block")
    private String block;

    @Basic(optional = false)
    @Column(nullable = false, name = "number")
    private int number;

    @Basic(optional = true)
    @Column(nullable = true, name = "accessible")
    private boolean accessible;

    @Basic(optional = true)
    @Column(nullable = true, name = "privilage")
    private int privilage;

    @JsonManagedReference
    @OneToMany(mappedBy = "roomReservation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reservation> reservations;

    public String getName() {
        return name;
    }

    public String getBuilding() {
        return building;
    }

    public String getBlock() {
        return block;
    }

    public int getNumber() {
        return number;
    }

    public boolean isAccessible() {
        return accessible;
    }

    public int getPrivilage() {
        return privilage;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setAccessible(boolean accessible) {
        this.accessible = accessible;
    }

    public void setPrivilage(int privilage) {
        this.privilage = privilage;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }
}
