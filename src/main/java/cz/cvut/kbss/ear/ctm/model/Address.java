package cz.cvut.kbss.ear.ctm.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "address",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {"city", "street", "house_number", "zip_code"})
        })
@NamedQueries({
        @NamedQuery(name = Address.FIND,
                query = "SELECT a FROM Address a " +
                        "WHERE :city = a.city " +
                        "AND :street = a.street " +
                        "AND :houseNumber = a.houseNumber " +
                        "AND :zipCode = a.zipCode")
})
public class Address extends AbstractEntity
{
    public static final String FIND = "Address.find";

    @Basic(optional = false)
    @Column(nullable = false, length = 30, name = "city")
    private String city;

    @Basic(optional = false)
    @Column(nullable = false, length = 30, name = "street")
    private String street;

    @Basic(optional = false)
    @Column(name = "house_number", nullable = false, length = 10)
    private String houseNumber;

    @Basic(optional = false)
    @Column(name = "zip_code", nullable = false, length = 5)
    private String zipCode;

    public Address()
    {  }

    public Address(String city, String street, String houseNumber, String zipCode)
    {
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
        this.zipCode = zipCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        if (city != null ? !city.equals(address.city) : address.city != null) return false;
        if (street != null ? !street.equals(address.street) : address.street != null) return false;
        if (houseNumber != null ? !houseNumber.equals(address.houseNumber) : address.houseNumber != null)
            return false;
        return zipCode != null ? zipCode.equals(address.zipCode) : address.zipCode == null;
    }
}
