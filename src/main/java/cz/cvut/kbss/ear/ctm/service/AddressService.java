package cz.cvut.kbss.ear.ctm.service;

import cz.cvut.kbss.ear.ctm.dao.AddressDao;
import cz.cvut.kbss.ear.ctm.dao.GenericDao;
import cz.cvut.kbss.ear.ctm.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AddressService extends AbstractRepositoryService<Address> {
    private final AddressDao dao;

    @Autowired
    public AddressService(AddressDao dao) {
        this.dao = dao;
    }

    @Override
    protected GenericDao<Address> getPrimaryDao() {
        return dao;
    }

    @Transactional(readOnly = true)
    public boolean exists(Address address) {
        return dao.find(address) != null;
    }

    @Transactional(readOnly = true)
    public Address find(Address address) {
        return dao.find(address);
    }
}
