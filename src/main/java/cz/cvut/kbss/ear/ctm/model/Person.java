package cz.cvut.kbss.ear.ctm.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.kbss.ear.ctm.model.enums.Role;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@Entity
@NamedQueries({
        @NamedQuery(name = Person.FIND_BY_EMAIL,
        query = "SELECT p FROM Person p WHERE p.email = :email"),
        @NamedQuery(name = Person.FIND_BY_BIRTH_NUMBER,
        query = "SELECT p FROM Person p WHERE p.birthNumber = :birthNumber")
})
public class Person extends AbstractEntity
{
    public final static String FIND_BY_EMAIL = "Person.findByEmail";
    public final static String FIND_BY_BIRTH_NUMBER = "Person.findByBirthNumber";

    @Basic
    @Column(nullable = false, unique = true, length = 10, name = "birthnumber")
    protected String birthNumber;

    @Basic(optional = false)
    @Column(nullable = false,unique = true, length = 40, name = "email")
    private String email;

    @Basic(optional = false)
    @Column(nullable = false, length = 30,name = "firstname")
    private String firstName;

    @Basic(optional = false)
    @Column(nullable = false, length = 30,name = "lastname")
    private String lastName;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Basic(optional = false)
    @Column(nullable = false, length = 255,name = "password")
    private String password;

    @Basic(optional = false)
    @Column(nullable = false, length = 40,name = "phonenumber")
    private String phoneNumber;

    @Enumerated(EnumType.ORDINAL)
    @Basic(optional = false)
    @Column(nullable = false, length = 1,name = "role")
    private Role role;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;

    @OneToMany(mappedBy = "personReservation", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reservation> reservations;

    public Person() {}

    public Person(String firstName, String lastName, String email, String phoneNumber, String birthNumber, String password, Role role, Address address)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
        this.birthNumber = birthNumber;
        this.role = role;
        this.address = address;
    }

    public String getUsername()
    {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName () {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Role getRole() {
        return role;
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public void setRole (Role role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setBirthNumber(String birthNumber) {
        this.birthNumber = birthNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthNumber() {
        return birthNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public void setId (Integer id) {
        super.setId(id);
    }

    @Override
    public Integer getId() {
        return super.getId();
    }

    /**
     * Encodes the person's password.
     *
     * @param encoder The password encoder to use
     */
    public void encodePassword(PasswordEncoder encoder) {
        Objects.requireNonNull(encoder);
        if (password != null) {
            this.password = encoder.encode(password);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (firstName != null ?     !firstName.equals(person.firstName)     : person.firstName != null)     return false;
        if (lastName != null ?      !lastName.equals(person.lastName)       : person.lastName != null)      return false;
        if (password != null ?      !password.equals(person.password)       : person.password != null)      return false;
        if (birthNumber != null ?   !birthNumber.equals(person.birthNumber) : person.birthNumber != null)   return false;
        if (phoneNumber != null ?   !phoneNumber.equals(person.phoneNumber) : person.phoneNumber != null)   return false;
        if (email != null ?         !email.equals(person.email)             : person.email != null)         return false;
        if (role != null ?          !role.equals(person.role)               : person.role != null)          return false;
        if (address != null ?       !address.equals(person.address)         : person.address != null)       return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (birthNumber != null ? birthNumber.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }
}
