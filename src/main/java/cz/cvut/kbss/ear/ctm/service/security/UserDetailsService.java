/**
 * Copyright (C) 2016 Czech Technical University in Prague
 * <p>
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details. You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package cz.cvut.kbss.ear.ctm.service.security;


import cz.cvut.kbss.ear.ctm.dao.PersonDao;
import cz.cvut.kbss.ear.ctm.exception.UsernameExistsException;
import cz.cvut.kbss.ear.ctm.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private PersonDao personDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Person person = loadPersonByUsername(username);
        if (person == null) {
            person = personDao.findByEmail(username);
        }

        return new cz.cvut.kbss.ear.ctm.security.model.UserDetails(person);
    }

    public Person loadPersonByUsername(String username) {
        Person person = personDao.findByEmail(username);
        if (person == null) {
            person = personDao.findByEmail(username);
        }
        return person;
    }

    public void verifyUniqueUsername(Person person) throws UsernameExistsException {
        Person abstractPerson = loadPersonByUsername(person.getUsername());
        if(abstractPerson != null && abstractPerson.getUsername().equals(person.getUsername()) && !abstractPerson.getId().equals(person.getId())) {
            throw new UsernameExistsException("Username " + abstractPerson.getUsername() + " already exists.");
        }
    }
}
