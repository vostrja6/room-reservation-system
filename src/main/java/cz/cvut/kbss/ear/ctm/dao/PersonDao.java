package cz.cvut.kbss.ear.ctm.dao;

import cz.cvut.kbss.ear.ctm.model.Person;
import org.springframework.stereotype.Repository;
import javax.persistence.NoResultException;

@Repository
public class PersonDao extends BaseDao<Person>
{
    public PersonDao()
    {
        super(Person.class);
    }

    public Person findByEmail(String email)
    {
        try {
            return em.createNamedQuery(Person.FIND_BY_EMAIL, Person.class).setParameter("email", email)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Person findByBirthNumber(String birthNumber)
    {
        try {
            return em.createNamedQuery(Person.FIND_BY_BIRTH_NUMBER, Person.class).setParameter("birthNumber", birthNumber)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}