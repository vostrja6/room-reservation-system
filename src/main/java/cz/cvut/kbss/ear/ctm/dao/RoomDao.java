package cz.cvut.kbss.ear.ctm.dao;

import cz.cvut.kbss.ear.ctm.dto.ReservationDto;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.model.Room;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Arrays;
import java.util.List;

@Repository
public class RoomDao extends BaseDao<Room>
{
    public RoomDao()
    {
        super(Room.class);
    }

    public List<Reservation> findDayRoomReservations(Integer roomId, String date) {
        try {
            String str_date = "'" + date +"'";
            List<Reservation> res = em.createNativeQuery(
                    "select * from reservation where reservation.date = " + str_date +
                                                " and reservation.room_id = " + roomId, Reservation.class
            ).getResultList();

            return res;
        }
        catch (NoResultException e) {
            return null;
        }
    }
}
