package cz.cvut.kbss.ear.ctm.service;

import cz.cvut.kbss.ear.ctm.dao.GenericDao;
import cz.cvut.kbss.ear.ctm.dao.RoomDao;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.model.Room;
import cz.cvut.kbss.ear.ctm.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.Iterator;
import java.util.List;

@Service
public class RoomService extends AbstractRepositoryService<Room> {

    private final SecurityUtils securityUtils;

    private final RoomDao roomDao;

    @Autowired
    public RoomService(SecurityUtils securityUtils, RoomDao roomDao) {
        this.roomDao = roomDao;
        this.securityUtils = securityUtils;
    }

    @Override
    protected GenericDao<Room> getPrimaryDao() {
        return this.roomDao;
    }

    public List<Room> getRooms()
    {
        return findAll();
    }

    public Room getRoom(Integer id) { return find(id); }

    public List<Reservation> getDayRoomReservations(Integer roomId, String date) {
        return roomDao.findDayRoomReservations(roomId, date);
    }
}
