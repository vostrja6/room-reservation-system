package cz.cvut.kbss.ear.ctm.dao;

import cz.cvut.kbss.ear.ctm.exception.PersistenceException;
import cz.cvut.kbss.ear.ctm.model.Address;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.Objects;

@Repository
public class AddressDao extends BaseDao<Address>
{
    public AddressDao()
    {
        super(Address.class);
    }

    /**
     * Finds {@link Address} instance if is in database.
     *
     * @param address
     * @return Instance, null if none matche
     */
    public Address find(Address address)
    {
//        try
//        {
//            return em.createNamedQuery(Address.FIND, Address.class)
//                    .setParameter("city", address.getCity())
//                    .setParameter("street", address.getStreet())
//                    .setParameter("houseNumber", address.getHouseNumber())
//                    .setParameter("zipCode", address.getZipCode())
//                    .getSingleResult();
//        }
//        catch (NoResultException e)
//        {
//            return null;
//        }
        return null;
    }

    @Override
    public void persist(Address entity) {
        Objects.requireNonNull(entity);
        try {
            if(this.find(entity) == null)
            {
                em.persist(entity);
            }
        } catch (RuntimeException e) {
            throw new PersistenceException(e);
        }
    }
}
