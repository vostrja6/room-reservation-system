package cz.cvut.kbss.ear.ctm.rest.reservation;

import cz.cvut.kbss.ear.ctm.dto.ReservationDto;
import cz.cvut.kbss.ear.ctm.model.Person;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.rest.mapper.DtoMapper;
import cz.cvut.kbss.ear.ctm.service.PersonService;
import cz.cvut.kbss.ear.ctm.service.ReservationService;
import cz.cvut.kbss.ear.ctm.service.RoomService;
import cz.cvut.kbss.ear.ctm.service.security.SecurityUtils;
import cz.cvut.kbss.ear.ctm.exception.AlreadyRemovedReservationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@PreAuthorize("hasAnyRole('USER', 'ADMIN')")
@RestController
@RequestMapping("/reservations")
public class ReservationController {
    private final ReservationService reservationService;

    private final RoomService roomService;

    private final PersonService personService;

    private final SecurityUtils securityUtils;

    private final DtoMapper dtoMapper;

    @Autowired
    public ReservationController(ReservationService reservationService, RoomService roomService, PersonService personService, DtoMapper dtoMapper, SecurityUtils securityUtils)
    {
        this.reservationService = reservationService;
        this.roomService = roomService;
        this.personService = personService;
        this.dtoMapper = dtoMapper;
        this.securityUtils = securityUtils;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservations()
    {
        return reservationService.findAll();
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value= "/myReservations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getMyReservations() {
        Person person = personService.findByEmail(securityUtils.getCurrentUser().getEmail());
        System.out.println("Person mail: " + person.getEmail());
        System.out.println("Person res: " + Arrays.toString(person.getReservations().toArray()));

        return person.getReservations();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeReservation(@PathVariable("id") Integer reservationId)
    {
        reservationService.removeReservation(reservationId);
    }

    @RequestMapping(value = "/confirm/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void confirmReservation(@PathVariable("id") Integer reservationId)
    {
        reservationService.confirmReservation(reservationId);
    }

    @RequestMapping(value = "/set/created/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setCreatedReservation(@PathVariable("id") Integer reservationId)
    {
        reservationService.setUpReservation(reservationId, 0);
    }

    @RequestMapping(value = "/set/waiting/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setWaitingReservation(@PathVariable("id") Integer reservationId)
    {
        reservationService.setUpReservation(reservationId, 1);
    }

    @RequestMapping(value = "/set/passed/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void setPassedReservation(@PathVariable("id") Integer reservationId)
    {
        reservationService.setUpReservation(reservationId, 3);
    }

    @PreAuthorize("hasAnyRole('USER', 'ADMIN')")
    @RequestMapping(value = "/date={date}/time={time}/room={roomId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Reservation findReservation(@PathVariable("date") String date, @PathVariable("time") String time, @PathVariable("roomId") Integer roomId)
    {
        System.out.println("in backend: find reservation: " + date + " time: " + time + " room: " + roomId);
        return reservationService.findReservation(date, time, roomId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void createSurgeryHour(@RequestBody ReservationDto reservationDto)
    {
        Reservation reservation = new Reservation();
        reservation.setDate(reservationDto.getDate());
        reservation.setStatus(reservationDto.getStatus());
        reservation.setTimeFrom(reservationDto.getTimeFrom());
        reservation.setTimeTo(reservationDto.getTimeTo());
        reservation.setPersonReservation(personService.find(reservationDto.getPersonId()));
        reservation.setRoomReservation(roomService.find(reservationDto.getRoomId()));

        reservationService.createReservation(reservation);
    }
}
