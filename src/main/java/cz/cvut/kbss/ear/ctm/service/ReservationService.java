package cz.cvut.kbss.ear.ctm.service;

import cz.cvut.kbss.ear.ctm.dao.GenericDao;
import cz.cvut.kbss.ear.ctm.dao.ReservationDao;
import cz.cvut.kbss.ear.ctm.exception.AlreadyAddedReservationException;
import cz.cvut.kbss.ear.ctm.exception.AlreadyRemovedReservationException;
import cz.cvut.kbss.ear.ctm.exception.AuthorizationException;
import cz.cvut.kbss.ear.ctm.exception.ReservationConflictException;
import cz.cvut.kbss.ear.ctm.model.Person;
import cz.cvut.kbss.ear.ctm.model.Reservation;
import cz.cvut.kbss.ear.ctm.model.Room;
import cz.cvut.kbss.ear.ctm.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReservationService extends AbstractRepositoryService<Reservation> {
    private final ReservationDao reservationDao;

    private final PersonService personService;

    private final RoomService roomService;

    private final SecurityUtils securityUtils;

    @Autowired
    public ReservationService(ReservationDao reservationDao, PersonService personService, RoomService roomService, SecurityUtils securityUtils) {
        this.reservationDao = reservationDao;
        this.personService = personService;
        this.roomService = roomService;
        this.securityUtils = securityUtils;
    }

    @Override
    protected GenericDao<Reservation> getPrimaryDao()
    {
        return reservationDao;
    }

    @Transactional
    public void createReservation(Reservation reservation) {

        Person person = personService.findByEmail(securityUtils.getCurrentUser().getEmail());
        reservation.setPersonReservation(person);

        List<Reservation> list = person.getReservations();
        if (list == null) {
            list = new ArrayList<>();
        }

        Room room = roomService.find(reservation.getRoomReservation().getId());
        List<Reservation> rooms = room.getReservations();
        if (rooms == null) {
            rooms = new ArrayList<>();
        }

        this.persist(reservation);
        list.add(reservation);
        rooms.add(reservation);
        person.setReservations(list);
        room.setReservations(rooms);
    }

    @Transactional
    public void removeReservation(Integer reservationId) {
        Reservation reservation = find(reservationId);
        Person person = personService.findByEmail(securityUtils.getCurrentUser().getEmail());

        if (reservation.getPersonReservation().getId().equals(person.getId())) {
            List<Reservation> list = person.getReservations();
            if (list != null) {
                list.remove(reservation);
            }
            person.setReservations(list);

            Room room = roomService.find(reservation.getRoomReservation().getId());
            List<Reservation> roomRes = room.getReservations();
            if (roomRes != null) {
                roomRes.remove(reservation);
            }
            room.setReservations(roomRes);

            this.remove(reservation);
        }
        else {
            throw new AuthorizationException("Modifying other user\'s account is forbidden.");
        }
    }

    @Transactional
    public void confirmReservation(Integer reservationId) {
        Reservation reservation = find(reservationId);
        Person person = personService.findByEmail(securityUtils.getCurrentUser().getEmail());

        if (reservation.getPersonReservation().getId().equals(person.getId())) {
            reservation.setStatus(2);
        }
        else {
            throw new AuthorizationException("Modifying other user\'s reservations is forbidden.");
        }
    }

    @Transactional
    public void setUpReservation(Integer reservationId, Integer status) {
        Reservation reservation = find(reservationId);
        Person person = personService.findByEmail(securityUtils.getCurrentUser().getEmail());

        if (reservation.getPersonReservation().getId().equals(person.getId())) {
            reservation.setStatus(status);
        }
        else {
            throw new AuthorizationException("Modifying other user\'s reservations is forbidden.");
        }
    }

    public Reservation findReservation(String date, String time, Integer roomId) {
        return reservationDao.findReservation(date, time, roomId);
    }

}
