package cz.cvut.kbss.ear.ctm.dao;

import cz.cvut.kbss.ear.ctm.model.Reservation;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ReservationDao extends BaseDao<Reservation>
{
    public ReservationDao()
    {
        super(Reservation.class);
    }

    public Reservation findReservation(String date, String time, Integer roomId) {
        try {
            String str_date = "'" + date +"'";
            String str_time = "'" + time +"'";

            List<Reservation> res = em.createNativeQuery(
                    "select * from reservation where reservation.date = " + str_date +
                            " and reservation.room_id = " + roomId +
                            " and reservation.timeFrom = " + str_time,
                    Reservation.class).getResultList();

            if (!res.isEmpty()) {
                return res.get(0);
            }

            return null;
        }
        catch (NoResultException e) {
            return null;
        }
    }
}