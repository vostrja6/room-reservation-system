package cz.cvut.kbss.ear.ctm.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "reservation")
@NamedQueries({
        @NamedQuery(name = Reservation.FIND_CONFLICT_RESERVATIONS,
                query = "SELECT s FROM Reservation s " +
                        "WHERE s.timeFrom BETWEEN :timeFrom AND :timeTo " +
                        "AND s.timeTo BETWEEN :timeFrom AND :timeTo " +
                        "AND :timeFrom BETWEEN s.timeFrom AND s.timeTo " +
                        "AND :timeTo BETWEEN s.timeFrom AND s.timeTo")})
//,
//@NamedQuery(name = Reservation.FIND_ROOM_RESERVATIONS,
//        query = "SELECT res FROM Reservation res" +
//                "WHERE res.room_id" + "= :room_id")
public class Reservation extends AbstractEntity
{
    public static final String FIND_CONFLICT_RESERVATIONS = "Person.findConflictReservation";
    public static final String FIND_ROOM_RESERVATIONS = "Room.findRoomReservations";

    @Basic(optional = false)
    @Column(nullable = false)
    private String date;

    @Basic(optional = false)
    @Column(nullable = false)
    private String timeFrom;

    @Basic(optional = false)
    @Column(nullable = false)
    private String timeTo;

    @Basic(optional = false)
    @Column(nullable = false)
    private Integer status;

    @JsonIgnore
    @ManyToOne(optional = false)
    @JoinColumn(name = "PERSON_ID")
    private Person personReservation;

    @JsonBackReference
    @ManyToOne(optional = false)
    @JoinColumn(name = "ROOM_ID")
    private Room roomReservation;

    public Reservation() {}

    public Reservation(String date, String timeFrom, String timeTo, Person person, Room room) {
        this.date = date;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
        this.personReservation = person;
        this.roomReservation = room;
    }

    //date format: 160719 ... 16.07.2019
    //time format: 15:30
    public void setDate(String date) {
        this.date = date;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setPersonReservation(Person personReservation) {
        this.personReservation = personReservation;
    }

    public void setRoomReservation(Room roomReservation) {
        this.roomReservation = roomReservation;
    }

    public String getDate() {
        return date;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public Integer getStatus() {
        return status;
    }

    public Person getPersonReservation() {
        return personReservation;
    }

    public Room getRoomReservation() {
        return roomReservation;
    }

    /*public void setDay(Integer dayInt)
    {
        this.day = dayInt.byteValue();
    }*/
}
