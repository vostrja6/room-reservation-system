# Room reservation system 

This application was created within Bachelor's thesis "Design and implementation of rooms reservation system". The main purpose for the application is implement environment for rooms reservation.  

## Getting Started

### Prerequisites

What things you need to install to use the software:

```
JDK 8
Git
Maven
Apache Tomcat
Node.js v6 or later 
```

### Installing

A step by step series of examples that tell you have to get a development env running

```
$ cd src/main/webapp
$ npm run build
$ npm start
```

Run your Tomcat Apache server:
(In case you use IntelliJ IDEA)

Edit Configurations -> Add Tomcat Server Local

Deployment -> Add Artifact: *ctmwar_exploded*.

## Built With

* [Bootstrap 3](https://getbootstrap.com/docs/3.3/) - Bootstrap 3

## Author

Jan Vostrý

## License

This project is licensed under GNU Licence - see the [LICENSE](LICENSE) file for details.
I've granted full privilage to this program from CTU in Prague.
